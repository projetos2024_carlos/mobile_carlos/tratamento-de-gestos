import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import CountMoviment from "./src/countMoviment";
import SlideMoviment from "./src/slideMoviment";
import Home from "./src/home";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={Home}/>
          <Stack.Screen name="CountMoviment" component={CountMoviment}/>
          <Stack.Screen name="SlideMoviment" component={SlideMoviment}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}



