import React, { useRef, useState } from "react";
import { View, Dimensions, PanResponder, Image, StyleSheet } from "react-native";

const SlideMoviment = () => {
  const img = [
    "https://lartbr.com.br/wp-content/uploads/2023/01/IMG_3564.jpg",
    "https://s2-autoesporte.glbimg.com/Hd0xOwD4BNi53gaqw3sWHuLqVAQ=/0x0:743x410/888x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_cf9d035bf26b4646b105bd958f32089d/internal_photos/bs/2022/t/M/NDRWeQRD2mBaDyyoGKLw/2023-porsche-911-gt3-rs-leaked-full-details-coming-august-17th-196018-1.jpg",
    "https://bluesky.cdn.imgeng.in/cogstock-images/ff2c1ecc-d4fb-4a46-b6a4-ddfcc483739e.jpg?migeng=/w_1200/"
  ];

  const [direction, setDirection] = useState(0);
  const screenWidth = Dimensions.get("window").width;
  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderRelease: (event, gestureState) => {
        if (gestureState.dx < -50) {
          setDirection((prevDirection) =>
            prevDirection === img.length - 1 ? 0 : prevDirection + 1
          );
        } else if (gestureState.dx > 50) {
          setDirection((prevDirection) =>
            prevDirection === 0 ? img.length - 1 : prevDirection - 1
          );
        }
      }
    })
  ).current;

  return (
    <View {...panResponder.panHandlers} style={styles.container}>
      <Image
        source={{ uri: img[direction] }}
        style={styles.image}
        resizeMode="cover"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    width: Dimensions.get("window").width,
  },
});

export default SlideMoviment;
