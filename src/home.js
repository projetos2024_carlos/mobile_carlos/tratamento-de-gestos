import React from "react";
import { View, Button } from "react-native";

const Home = ({navigation}) => {
   const Count = () =>{
      navigation.navigate('CountMoviment')
   }
   const Images = () =>{
      navigation.navigate('SlideMoviment')
   }
   return(
      <View>
         <Button title="Count" onPress={Count}/>
         <Button title="Imagens" onPress={Images}/>
      </View>
   )
}

export default Home