import React, { useRef, useEffect, useState } from "react";
import { View, Dimensions, PanResponder, Button, Text, StyleSheet } from "react-native";


const CountMoviment = () => {

   const [count, setCount] = useState(0);
   const screenHeight = Dimensions.get("window").height;
   const gestureThreshold = screenHeight * 0.25; // valor a ser atingido,quando esse valor for atingido ele sera contado 
   const panResponder = useRef(
      PanResponder.create({
         onStartShouldSetPanResponder: () => true,
         onPanResponderMove: (event, gestureState) => { },
         onPanResponderRelease: (event, gestureState) => {
            if (gestureState.dy < -gestureThreshold) {
               setCount((prevCount) => prevCount + 1);//pega o valor que foi pré renderizado no count e soma mais um
            }
         },

      })

   ).current; //pra conseguir acessar o valor atual do objeto 
   return (
      <View
         {...panResponder.panHandlers}
         style={styles.container}>
         <Text style={styles.text}>Valor do Contador:{count}</Text>
      </View>
   )
};


const styles = StyleSheet.create({
   container: {
      backgroundColor: "biege",
      height: 1080,
      justifyContent: "center",
      flex: 1,
      alignItems: "center",
   },
   text: {
      fontSize: 20,
      fontWeight: "bold",
   },

});

export default CountMoviment;
